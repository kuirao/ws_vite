// vite.config.ts
import { defineConfig } from "file:///E:/T/project/ws/client_vite/node_modules/vite/dist/node/index.js";
import vue from "file:///E:/T/project/ws/client_vite/node_modules/@vitejs/plugin-vue/dist/index.mjs";
import { resolve } from "path";
import AutoImport from "file:///E:/T/project/ws/client_vite/node_modules/unplugin-auto-import/dist/vite.js";
import Components from "file:///E:/T/project/ws/client_vite/node_modules/unplugin-vue-components/dist/vite.js";
import { ElementPlusResolver } from "file:///E:/T/project/ws/client_vite/node_modules/unplugin-vue-components/dist/resolvers.js";
var __vite_injected_original_dirname = "E:\\T\\project\\ws\\client_vite";
var vite_config_default = defineConfig({
  server: {
    proxy: {
      "/xigua": {
        target: "https://www.ixigua.com",
        ws: true,
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/xigua/, ""),
        bypass(req, res, options) {
          const proxyURL = options.target + options.rewrite(req.url);
          res.setHeader("x-req-proxyURL", proxyURL);
        }
      },
      "/api": {
        target: "http://49.234.58.117:55556",
        // target: "http://localhost:55556",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
        bypass(req, res, options) {
          const proxyURL = options.target + options.rewrite(req.url);
          res.setHeader("x-req-proxyURL", proxyURL);
        }
      },
      "/ws": {
        target: "ws://localhost:55555",
        ws: true,
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/ws/, ""),
        bypass(req, res, options) {
          const proxyURL = options.target + options.rewrite(req.url);
          res.setHeader("x-req-proxyURL", proxyURL);
        }
      }
    },
    host: "0.0.0.0",
    // 主机名
    port: 5517,
    // 端口号
    open: true
    // 启动时是否打开浏览器
  },
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()]
    }),
    Components({
      resolvers: [ElementPlusResolver()]
    })
  ],
  resolve: {
    alias: {
      "@": resolve(__vite_injected_original_dirname, "src"),
      // 路径别名
      "@assets": resolve(__vite_injected_original_dirname, "src/assets")
    },
    extensions: [".js", ".json", ".ts"]
    // 使用路径别名时想要省略的后缀名，可以自己 增减
  },
  base: "./",
  // 打包路径
  build: {
    outDir: "dist",
    // 输出路径
    assetsDir: "assets",
    // 静态资源路径
    emptyOutDir: true,
    // 构建前先清空输出目录
    minify: "terser",
    // 压缩
    sourcemap: false,
    // 生产环境是否生成 sourcemap
    target: "es2016",
    // 编译目标
    rollupOptions: {
      input: {
        main: resolve(__vite_injected_original_dirname, "index.html")
        // 入口文件
      }
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "@/styles/variables.scss";`
        // 全局变量
      }
    }
  }
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJFOlxcXFxUXFxcXHByb2plY3RcXFxcd3NcXFxcY2xpZW50X3ZpdGVcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZmlsZW5hbWUgPSBcIkU6XFxcXFRcXFxccHJvamVjdFxcXFx3c1xcXFxjbGllbnRfdml0ZVxcXFx2aXRlLmNvbmZpZy50c1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9pbXBvcnRfbWV0YV91cmwgPSBcImZpbGU6Ly8vRTovVC9wcm9qZWN0L3dzL2NsaWVudF92aXRlL3ZpdGUuY29uZmlnLnRzXCI7aW1wb3J0IHsgZGVmaW5lQ29uZmlnIH0gZnJvbSBcInZpdGVcIjtcclxuaW1wb3J0IHZ1ZSBmcm9tIFwiQHZpdGVqcy9wbHVnaW4tdnVlXCI7XHJcbmltcG9ydCB7IHJlc29sdmUgfSBmcm9tIFwicGF0aFwiO1xyXG5cclxuaW1wb3J0IEF1dG9JbXBvcnQgZnJvbSBcInVucGx1Z2luLWF1dG8taW1wb3J0L3ZpdGVcIjtcclxuaW1wb3J0IENvbXBvbmVudHMgZnJvbSBcInVucGx1Z2luLXZ1ZS1jb21wb25lbnRzL3ZpdGVcIjtcclxuaW1wb3J0IHsgRWxlbWVudFBsdXNSZXNvbHZlciB9IGZyb20gXCJ1bnBsdWdpbi12dWUtY29tcG9uZW50cy9yZXNvbHZlcnNcIjtcclxuLy8gaW1wb3J0IHB4MnJlbSBmcm9tIFwicG9zdGNzcy1weDJyZW1cIjtcclxuLy8gXHU1RjE1XHU1MTY1XHU3QjQ5XHU2QkQ0XHU5MDAyXHU5MTREXHU2M0QyXHU0RUY2XHJcbi8vIGNvbnN0IHB4MnJlbSA9IHJlcXVpcmUoXCJwb3N0Y3NzLXB4MnJlbVwiKTtcclxuXHJcbi8vIFx1OTE0RFx1N0Y2RVx1NTdGQVx1NjcyQ1x1NTkyN1x1NUMwRlxyXG4vLyBjb25zdCBwb3N0Y3NzID0gcHgycmVtKHtcclxuLy8gICAvLyBcdTU3RkFcdTUxQzZcdTU5MjdcdTVDMEYgYmFzZVNpemVcdUZGMENcdTk3MDBcdTg5ODFcdTU0OENyZW0uanNcdTRFMkRcdTc2RjhcdTU0MENcclxuLy8gICByZW1Vbml0OiAxNixcclxuLy8gfSk7XHJcblxyXG5cclxuLy8gaHR0cHM6Ly92aXRlanMuZGV2L2NvbmZpZy9cclxuZXhwb3J0IGRlZmF1bHQgZGVmaW5lQ29uZmlnKHtcclxuICBzZXJ2ZXI6IHtcclxuICAgIHByb3h5OiB7XHJcbiAgICAgIFwiL3hpZ3VhXCI6IHtcclxuICAgICAgICB0YXJnZXQ6IFwiaHR0cHM6Ly93d3cuaXhpZ3VhLmNvbVwiLFxyXG4gICAgICAgIHdzOiB0cnVlLFxyXG4gICAgICAgIGNoYW5nZU9yaWdpbjogdHJ1ZSxcclxuICAgICAgICByZXdyaXRlOiAocGF0aCkgPT4gcGF0aC5yZXBsYWNlKC9eXFwveGlndWEvLCBcIlwiKSxcclxuICAgICAgICBieXBhc3MocmVxLCByZXMsIG9wdGlvbnM6IGFueSk6IHZvaWQge1xyXG4gICAgICAgICAgY29uc3QgcHJveHlVUkwgPSBvcHRpb25zLnRhcmdldCArIG9wdGlvbnMucmV3cml0ZShyZXEudXJsKTtcclxuICAgICAgICAgIHJlcy5zZXRIZWFkZXIoXCJ4LXJlcS1wcm94eVVSTFwiLCBwcm94eVVSTCk7IC8vIFx1NUMwNlx1NzcxRlx1NUI5RVx1OEJGN1x1NkM0Mlx1NTczMFx1NTc0MFx1OEJCRVx1N0Y2RVx1NTIzMFx1NTRDRFx1NUU5NFx1NTkzNFx1NEUyRFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH0sXHJcbiAgICAgIFwiL2FwaVwiOiB7XHJcbiAgICAgICAgdGFyZ2V0OiBcImh0dHA6Ly80OS4yMzQuNTguMTE3OjU1NTU2XCIsXHJcbiAgICAgICAgLy8gdGFyZ2V0OiBcImh0dHA6Ly9sb2NhbGhvc3Q6NTU1NTZcIixcclxuICAgICAgICBjaGFuZ2VPcmlnaW46IHRydWUsXHJcbiAgICAgICAgcmV3cml0ZTogKHBhdGgpID0+IHBhdGgucmVwbGFjZSgvXlxcL2FwaS8sIFwiXCIpLFxyXG4gICAgICAgIGJ5cGFzcyhyZXEsIHJlcywgb3B0aW9uczogYW55KTogdm9pZCB7XHJcbiAgICAgICAgICBjb25zdCBwcm94eVVSTCA9IG9wdGlvbnMudGFyZ2V0ICsgb3B0aW9ucy5yZXdyaXRlKHJlcS51cmwpO1xyXG4gICAgICAgICAgcmVzLnNldEhlYWRlcihcIngtcmVxLXByb3h5VVJMXCIsIHByb3h5VVJMKTsgLy8gXHU1QzA2XHU3NzFGXHU1QjlFXHU4QkY3XHU2QzQyXHU1NzMwXHU1NzQwXHU4QkJFXHU3RjZFXHU1MjMwXHU1NENEXHU1RTk0XHU1OTM0XHU0RTJEXHJcbiAgICAgICAgfSxcclxuICAgICAgfSxcclxuICAgICAgXCIvd3NcIjoge1xyXG4gICAgICAgIHRhcmdldDogXCJ3czovL2xvY2FsaG9zdDo1NTU1NVwiLFxyXG4gICAgICAgIHdzOiB0cnVlLFxyXG4gICAgICAgIGNoYW5nZU9yaWdpbjogdHJ1ZSxcclxuICAgICAgICByZXdyaXRlOiAocGF0aCkgPT4gcGF0aC5yZXBsYWNlKC9eXFwvd3MvLCBcIlwiKSxcclxuICAgICAgICBieXBhc3MocmVxLCByZXMsIG9wdGlvbnM6IGFueSk6IHZvaWQge1xyXG4gICAgICAgICAgY29uc3QgcHJveHlVUkwgPSBvcHRpb25zLnRhcmdldCArIG9wdGlvbnMucmV3cml0ZShyZXEudXJsKTtcclxuICAgICAgICAgIHJlcy5zZXRIZWFkZXIoXCJ4LXJlcS1wcm94eVVSTFwiLCBwcm94eVVSTCk7IC8vIFx1NUMwNlx1NzcxRlx1NUI5RVx1OEJGN1x1NkM0Mlx1NTczMFx1NTc0MFx1OEJCRVx1N0Y2RVx1NTIzMFx1NTRDRFx1NUU5NFx1NTkzNFx1NEUyRFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgaG9zdDogXCIwLjAuMC4wXCIsIC8vIFx1NEUzQlx1NjczQVx1NTQwRFxyXG4gICAgcG9ydDogNTUxNywgLy8gXHU3QUVGXHU1M0UzXHU1M0Y3XHJcbiAgICBvcGVuOiB0cnVlLCAvLyBcdTU0MkZcdTUyQThcdTY1RjZcdTY2MkZcdTU0MjZcdTYyNTNcdTVGMDBcdTZENEZcdTg5QzhcdTU2NjhcclxuICB9LFxyXG4gIHBsdWdpbnM6IFtcclxuICAgIHZ1ZSgpLFxyXG4gICAgQXV0b0ltcG9ydCh7XHJcbiAgICAgIHJlc29sdmVyczogW0VsZW1lbnRQbHVzUmVzb2x2ZXIoKV0sXHJcbiAgICB9KSxcclxuICAgIENvbXBvbmVudHMoe1xyXG4gICAgICByZXNvbHZlcnM6IFtFbGVtZW50UGx1c1Jlc29sdmVyKCldLFxyXG4gICAgfSksXHJcbiAgXSxcclxuICByZXNvbHZlOiB7XHJcbiAgICBhbGlhczoge1xyXG4gICAgICBcIkBcIjogcmVzb2x2ZShfX2Rpcm5hbWUsIFwic3JjXCIpLCAvLyBcdThERUZcdTVGODRcdTUyMkJcdTU0MERcclxuICAgICAgXCJAYXNzZXRzXCI6IHJlc29sdmUoX19kaXJuYW1lLCBcInNyYy9hc3NldHNcIiksXHJcbiAgICB9LFxyXG4gICAgZXh0ZW5zaW9uczogW1wiLmpzXCIsIFwiLmpzb25cIiwgXCIudHNcIl0sIC8vIFx1NEY3Rlx1NzUyOFx1OERFRlx1NUY4NFx1NTIyQlx1NTQwRFx1NjVGNlx1NjBGM1x1ODk4MVx1NzcwMVx1NzU2NVx1NzY4NFx1NTQwRVx1N0YwMFx1NTQwRFx1RkYwQ1x1NTNFRlx1NEVFNVx1ODFFQVx1NURGMSBcdTU4OUVcdTUxQ0ZcclxuICB9LFxyXG4gIGJhc2U6IFwiLi9cIiwgLy8gXHU2MjUzXHU1MzA1XHU4REVGXHU1Rjg0XHJcbiAgYnVpbGQ6IHtcclxuICAgIG91dERpcjogXCJkaXN0XCIsIC8vIFx1OEY5M1x1NTFGQVx1OERFRlx1NUY4NFxyXG4gICAgYXNzZXRzRGlyOiBcImFzc2V0c1wiLCAvLyBcdTk3NTlcdTYwMDFcdThENDRcdTZFOTBcdThERUZcdTVGODRcclxuICAgIGVtcHR5T3V0RGlyOiB0cnVlLCAvLyBcdTY3ODRcdTVFRkFcdTUyNERcdTUxNDhcdTZFMDVcdTdBN0FcdThGOTNcdTUxRkFcdTc2RUVcdTVGNTVcclxuICAgIG1pbmlmeTogXCJ0ZXJzZXJcIiwgLy8gXHU1MzhCXHU3RjI5XHJcbiAgICBzb3VyY2VtYXA6IGZhbHNlLCAvLyBcdTc1MUZcdTRFQTdcdTczQUZcdTU4ODNcdTY2MkZcdTU0MjZcdTc1MUZcdTYyMTAgc291cmNlbWFwXHJcbiAgICB0YXJnZXQ6IFwiZXMyMDE2XCIsIC8vIFx1N0YxNlx1OEJEMVx1NzZFRVx1NjgwN1xyXG4gICAgcm9sbHVwT3B0aW9uczoge1xyXG4gICAgICBpbnB1dDoge1xyXG4gICAgICAgIG1haW46IHJlc29sdmUoX19kaXJuYW1lLCBcImluZGV4Lmh0bWxcIiksIC8vIFx1NTE2NVx1NTNFM1x1NjU4N1x1NEVGNlxyXG4gICAgICB9LFxyXG4gICAgfSxcclxuICB9LFxyXG4gIGNzczoge1xyXG4gICAgcHJlcHJvY2Vzc29yT3B0aW9uczoge1xyXG4gICAgICBzY3NzOiB7XHJcbiAgICAgICAgYWRkaXRpb25hbERhdGE6IGBAaW1wb3J0IFwiQC9zdHlsZXMvdmFyaWFibGVzLnNjc3NcIjtgLCAvLyBcdTUxNjhcdTVDNDBcdTUzRDhcdTkxQ0ZcclxuICAgICAgfSxcclxuICAgIH0sXHJcbiAgfSxcclxuXHJcbn0pO1xyXG4iXSwKICAibWFwcGluZ3MiOiAiO0FBQTZRLFNBQVMsb0JBQW9CO0FBQzFTLE9BQU8sU0FBUztBQUNoQixTQUFTLGVBQWU7QUFFeEIsT0FBTyxnQkFBZ0I7QUFDdkIsT0FBTyxnQkFBZ0I7QUFDdkIsU0FBUywyQkFBMkI7QUFOcEMsSUFBTSxtQ0FBbUM7QUFtQnpDLElBQU8sc0JBQVEsYUFBYTtBQUFBLEVBQzFCLFFBQVE7QUFBQSxJQUNOLE9BQU87QUFBQSxNQUNMLFVBQVU7QUFBQSxRQUNSLFFBQVE7QUFBQSxRQUNSLElBQUk7QUFBQSxRQUNKLGNBQWM7QUFBQSxRQUNkLFNBQVMsQ0FBQyxTQUFTLEtBQUssUUFBUSxZQUFZLEVBQUU7QUFBQSxRQUM5QyxPQUFPLEtBQUssS0FBSyxTQUFvQjtBQUNuQyxnQkFBTSxXQUFXLFFBQVEsU0FBUyxRQUFRLFFBQVEsSUFBSSxHQUFHO0FBQ3pELGNBQUksVUFBVSxrQkFBa0IsUUFBUTtBQUFBLFFBQzFDO0FBQUEsTUFDRjtBQUFBLE1BQ0EsUUFBUTtBQUFBLFFBQ04sUUFBUTtBQUFBO0FBQUEsUUFFUixjQUFjO0FBQUEsUUFDZCxTQUFTLENBQUMsU0FBUyxLQUFLLFFBQVEsVUFBVSxFQUFFO0FBQUEsUUFDNUMsT0FBTyxLQUFLLEtBQUssU0FBb0I7QUFDbkMsZ0JBQU0sV0FBVyxRQUFRLFNBQVMsUUFBUSxRQUFRLElBQUksR0FBRztBQUN6RCxjQUFJLFVBQVUsa0JBQWtCLFFBQVE7QUFBQSxRQUMxQztBQUFBLE1BQ0Y7QUFBQSxNQUNBLE9BQU87QUFBQSxRQUNMLFFBQVE7QUFBQSxRQUNSLElBQUk7QUFBQSxRQUNKLGNBQWM7QUFBQSxRQUNkLFNBQVMsQ0FBQyxTQUFTLEtBQUssUUFBUSxTQUFTLEVBQUU7QUFBQSxRQUMzQyxPQUFPLEtBQUssS0FBSyxTQUFvQjtBQUNuQyxnQkFBTSxXQUFXLFFBQVEsU0FBUyxRQUFRLFFBQVEsSUFBSSxHQUFHO0FBQ3pELGNBQUksVUFBVSxrQkFBa0IsUUFBUTtBQUFBLFFBQzFDO0FBQUEsTUFDRjtBQUFBLElBQ0Y7QUFBQSxJQUNBLE1BQU07QUFBQTtBQUFBLElBQ04sTUFBTTtBQUFBO0FBQUEsSUFDTixNQUFNO0FBQUE7QUFBQSxFQUNSO0FBQUEsRUFDQSxTQUFTO0FBQUEsSUFDUCxJQUFJO0FBQUEsSUFDSixXQUFXO0FBQUEsTUFDVCxXQUFXLENBQUMsb0JBQW9CLENBQUM7QUFBQSxJQUNuQyxDQUFDO0FBQUEsSUFDRCxXQUFXO0FBQUEsTUFDVCxXQUFXLENBQUMsb0JBQW9CLENBQUM7QUFBQSxJQUNuQyxDQUFDO0FBQUEsRUFDSDtBQUFBLEVBQ0EsU0FBUztBQUFBLElBQ1AsT0FBTztBQUFBLE1BQ0wsS0FBSyxRQUFRLGtDQUFXLEtBQUs7QUFBQTtBQUFBLE1BQzdCLFdBQVcsUUFBUSxrQ0FBVyxZQUFZO0FBQUEsSUFDNUM7QUFBQSxJQUNBLFlBQVksQ0FBQyxPQUFPLFNBQVMsS0FBSztBQUFBO0FBQUEsRUFDcEM7QUFBQSxFQUNBLE1BQU07QUFBQTtBQUFBLEVBQ04sT0FBTztBQUFBLElBQ0wsUUFBUTtBQUFBO0FBQUEsSUFDUixXQUFXO0FBQUE7QUFBQSxJQUNYLGFBQWE7QUFBQTtBQUFBLElBQ2IsUUFBUTtBQUFBO0FBQUEsSUFDUixXQUFXO0FBQUE7QUFBQSxJQUNYLFFBQVE7QUFBQTtBQUFBLElBQ1IsZUFBZTtBQUFBLE1BQ2IsT0FBTztBQUFBLFFBQ0wsTUFBTSxRQUFRLGtDQUFXLFlBQVk7QUFBQTtBQUFBLE1BQ3ZDO0FBQUEsSUFDRjtBQUFBLEVBQ0Y7QUFBQSxFQUNBLEtBQUs7QUFBQSxJQUNILHFCQUFxQjtBQUFBLE1BQ25CLE1BQU07QUFBQSxRQUNKLGdCQUFnQjtBQUFBO0FBQUEsTUFDbEI7QUFBQSxJQUNGO0FBQUEsRUFDRjtBQUVGLENBQUM7IiwKICAibmFtZXMiOiBbXQp9Cg==
