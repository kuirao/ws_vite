import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";

import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
// import px2rem from "postcss-px2rem";
// 引入等比适配插件
// const px2rem = require("postcss-px2rem");

// 配置基本大小
// const postcss = px2rem({
//   // 基准大小 baseSize，需要和rem.js中相同
//   remUnit: 16,
// });


// https://vitejs.dev/config/
export default defineConfig({
  server: {
    proxy: {
      "/xigua": {
        target: "https://www.ixigua.com",
        ws: true,
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/xigua/, ""),
        bypass(req, res, options: any): void {
          const proxyURL = options.target + options.rewrite(req.url);
          res.setHeader("x-req-proxyURL", proxyURL); // 将真实请求地址设置到响应头中
        },
      },
      "/api": {
        target: "http://49.234.58.117:55556",
        // target: "http://localhost:55556",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
        bypass(req, res, options: any): void {
          const proxyURL = options.target + options.rewrite(req.url);
          res.setHeader("x-req-proxyURL", proxyURL); // 将真实请求地址设置到响应头中
        },
      },
      "/ws": {
        target: "ws://localhost:55555",
        ws: true,
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/ws/, ""),
        bypass(req, res, options: any): void {
          const proxyURL = options.target + options.rewrite(req.url);
          res.setHeader("x-req-proxyURL", proxyURL); // 将真实请求地址设置到响应头中
        },
      },
    },
    host: "0.0.0.0", // 主机名
    port: 5517, // 端口号
    open: true, // 启动时是否打开浏览器
  },
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
  resolve: {
    alias: {
      "@": resolve(__dirname, "src"), // 路径别名
      "@assets": resolve(__dirname, "src/assets"),
    },
    extensions: [".js", ".json", ".ts"], // 使用路径别名时想要省略的后缀名，可以自己 增减
  },
  base: "./", // 打包路径
  build: {
    outDir: "dist", // 输出路径
    assetsDir: "assets", // 静态资源路径
    emptyOutDir: true, // 构建前先清空输出目录
    minify: "terser", // 压缩
    sourcemap: false, // 生产环境是否生成 sourcemap
    target: "es2016", // 编译目标
    rollupOptions: {
      input: {
        main: resolve(__dirname, "index.html"), // 入口文件
      },
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        api: "modern-compiler" ,// or "modern", "legacy"
        // additionalData: `@import "@/styles/variables.scss";`, // 全局变量
        importers: [
          // ...
        ],
        silenceDeprecations: ['legacy-js-api'] // 禁用scss警告
      },
    },
  },

});
