import User, { UserInfo } from '@/utils/user'

import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', {
  state: () => ({
    userInfo: User.instance.getUserInfo() || {}
  }),
  getters: {
    getUserInfo(state):UserInfo{
      return state.userInfo
    }
  },
  actions: {
    setUserInfo(userInfo:UserInfo){
      this.userInfo = userInfo
      User.instance.saveUserInfo(userInfo)
    },
    clearUserInfo(){
      this.userInfo = {}
      User.instance.clearUserInfo()
    },
  },
})