import { defineStore } from 'pinia'

export const useTheme = defineStore('theme', {
  state: () => ({
    theme: localStorage.getItem('theme') || 'light'
  }),
  getters: {
    getTheme(state):string{
      return state.theme
    }
  },
  actions: {
    setTheme(theme:string){
      this.theme = theme
      localStorage.setItem('theme', theme)
    },
    clearTheme(){
      this.theme = 'light'
    },
  },
})