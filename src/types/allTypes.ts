export interface Room {
  room_id: number
  room_name: string
  room_pic: string
}