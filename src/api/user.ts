import  request  from '@/utils/require';

/**
 * 登录
 *  */ 
export function userLogin(data: any) {
  return request({
    url: '/user/login',
    method: 'post',
    data,
  })
}

// 注册
export function userRegister(data: any) {
  return request({
    url: '/user/register',
    method: 'post',
    data,
  })
}


//获取图片验证码
export function getImgCode() {
  return request({
    url: '/user/code',
    method: 'get',
  })
}

/**
 * 获取用户信息
 */
export function getUserInfo(params: any) {
  return request({
    url: '/user/getUserInfo',
    method: 'get',
    params
  })
}

/**
 * 修改用户信息
 */
export function updateUserInfo(data: any) {
  return request({
    url: '/user/upDateById',
    method: 'post',
    data
  })
}