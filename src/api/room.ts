import request from "@/utils/require";

export function getRoomList(params: { user_id?: string }) {
  return request({ url: "/room/getRoomList", method: "GET", params });
}
