import CryptoJS from "crypto-js";
/**
 * SHA256 加密
 * @param str 加密字符串
 * @returns 加密后的字符串
 */
function sha256(str: string): string {
  return CryptoJS.SHA256(str).toString();
}

export default sha256;