//处理axios请求
import axios from "axios";
import { ElMessage } from "element-plus";

const instance = axios.create({
  baseURL: "/api",
  timeout: 10000,
});
//处理请求
instance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

//处理响应
instance.interceptors.response.use(
  (response) => {
    if (response.status === 200 || response.data.code === 200) {
      return response.data;
    } else if (response.status === 400 || response.data.code === 400) {
      ElMessage.error(response.data.msg);
      return Promise.reject(response.data);
    } else if (response.status === 500 || response.data.code === 500) {
      ElMessage.error("服务器错误");
      return Promise.reject(response.data);
    } else {
      return response.data;
    }
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance; //导出axios实例
