import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  { path: '/', redirect: { name: 'login' } },
  {
    path: '/login',
    name: 'login',
    component: () => import("@/views/login/index.vue")
  },
  {
    path:'/test',
    name:'test',
    component:()=>import("@/views/login/index.vue")
  },
  {
    path: '/chat',
    name: 'chat',
    component: () => import("@/views/chat/index.vue"),
    meta: {
      title: '聊天室',
      requiresAuth: true
    },
    children: [
      {
        path: 'main',
        name: 'main',
        component: () => import("@/views/chat/main.vue"),
        meta: {
          title: '聊天室',
          requiresAuth: true
        },
      },
      {
        path: 'userSet',
        name: 'userSet',
        component: () => import("@/views/user/index.vue"),
        meta: {
          requiresAuth: true
        }
      },
    ]
  },
]

const router = createRouter({
  // history: createWebHashHistory(),//线上使用这个
  history: createWebHistory(), 
  routes
})

router.beforeEach((to, from, next) => {
  console.log(to.name)
  next()
})

export default router