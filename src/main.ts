import { createApp } from "vue";
import "./style.css";
import './utils/rem'

import App from "./App.vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import zh from "element-plus/es/locale/lang/zh-cn";

import router from "@/route/index";
import { createPinia } from "pinia";

// 全局挂载three
// window.THREE = THREE;

createApp(App)
  .use(ElementPlus,{locale:zh})
  .use(router)
  .use(createPinia())

  .mount("#app");





